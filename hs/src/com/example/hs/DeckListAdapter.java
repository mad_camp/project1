package com.example.hs;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DeckListAdapter extends ArrayAdapter<String> {
	private Context context;
	private ArrayList<String> list;
	private LayoutInflater inflater;
	private DB_score db_score;
	
	public DeckListAdapter(Context context, ArrayList<String> list, DB_score db){
		super(context, 0, list);
		this.context = context;
		this.list = list;
		this.inflater = LayoutInflater.from(context);
		this.db_score = db;
		//Log.i("aa","aa");
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.list_row_2, null);
		}
		
		SQLiteDatabase db = db_score.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM db_deck WHERE name='"+getItem(position)+ "'", null);
		int win, draw, lose;
    	win=0;
    	draw=0;
    	lose=0;
    	
    	if (c != null){
    		if (c.getCount() != 0){
	    		c.moveToFirst();
	        	for (int i=0; i<9; i++){
	        		win += c.getInt(3*i+3);
	        		draw += c.getInt(3*i+4);
	        		lose += c.getInt(3*i+5);
	        	}
    		}
    	}
    	
    	Integer win_sum = new Integer(win); 
    	Integer draw_sum = new Integer(draw); 
    	Integer lose_sum = new Integer(lose); 
    	Integer All = new Integer(win_sum.intValue()+draw_sum.intValue()+lose_sum.intValue());
    	Float win_rate;
    	if(All.intValue()!=0){
    		win_rate = new Float(100*(win_sum.floatValue()/All.floatValue()));
    	}
    	else{
    		win_rate = new Float(0);
    	}
    	Integer result_rate = new Integer(win_rate.intValue());
    	
    	
		String item = getItem(position);
		
		TextView name = (TextView)convertView.findViewById(R.id.name);
		if (item!=null ||item!="") name.setText(item);
		TextView w = (TextView)convertView.findViewById(R.id.win);
		w.setText(win_sum.toString());
		TextView d = (TextView)convertView.findViewById(R.id.draw);
		d.setText(draw_sum.toString());
		TextView l = (TextView)convertView.findViewById(R.id.lose);
		l.setText(lose_sum.toString());
		TextView r = (TextView)convertView.findViewById(R.id.all_rate);
		r.setText(result_rate.toString()+"%");
		db.close();
		
		return convertView;
	}
}
