package com.example.hs;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DB_score extends SQLiteOpenHelper{

	public DB_score(Context context) {
		super(context, "db_score", null, 4);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE db_deck" + "("
				+ " _id integer PRIMARY KEY autoincrement,"	
				+ " name text,"
				+ " hero text,"
				+ " h1w integer, h1d integer, h1l integer,"
				+ " h2w integer, h2d integer, h2l integer,"
				+ " h3w integer, h3d integer, h3l integer,"
				+ " h4w integer, h4d integer, h4l integer,"
				+ " h5w integer, h5d integer, h5l integer,"
				+ " h6w integer, h6d integer, h6l integer,"
				+ " h7w integer, h7d integer, h7l integer,"
				+ " h8w integer, h8d integer, h8l integer,"
				+ " h9w integer, h9d integer, h9l integer)"
				    );
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		System.out.println("aaaaaa");
		db.execSQL("DROP TABLE IF EXISTS db_deck");
		onCreate(db);
	}

}
