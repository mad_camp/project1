package com.example.hs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.AssetManager.AssetInputStream;
import android.os.AsyncTask;


public class GetXMLTask4 extends AsyncTask<String, Void, String> {
	private Context context;
	private ArrayList<String> rank_deck;
	private ArrayList<String> rank_deck_url;
	private ArrayList<String> rank_deck_temp;

	public GetXMLTask4(Context context, ArrayList<String> rank_deck, ArrayList<String> rank_deck_url){
		this.context = context;
		this.rank_deck = rank_deck;
		this.rank_deck_url = rank_deck_url;
		rank_deck_temp = new ArrayList<String>();
	}

    @Override
    protected String doInBackground(String... urls) {
    	String s=null;
        for (String url : urls) {
        	s = downHtml(url);
        }
        return s;
	}
	private String downHtml(String url) {
		StringBuilder html = new StringBuilder();

		try {
			URL u = new URL(url);
			HttpURLConnection conn= (HttpURLConnection)u.openConnection();
			if (conn!=null){
				//conn.setConnectTimeout(10000);
				conn.connect();
				InputStreamReader isr = new InputStreamReader(conn.getInputStream(), "euc-kr");
				BufferedReader br = new BufferedReader(isr);
				for(;;){
					String line = br.readLine();
					if (line == null) break;
					html.append(line+"\n");
				}
				br.close();

			}
			conn.disconnect();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		String result = html.toString();
		//System.out.println(result);
		
		int start = 0;
		int s, e;
		
		while(true){
			s = result.indexOf("img class=\"job\"", start);
			start = s+1;
			
			int temp=0;
			
			if (s == -1) break;
			for (int i=0; i<4; i++){
	    		
				s = result.indexOf(">", s);
	    		if (i==1) temp=s;
				s++;
			}

			s++;
			if (result.substring(s-6, s).equals("span> ")){
				e = result.indexOf("</div", s);
				//System.out.println(result.substring(s, e));
				rank_deck_temp.add(result.substring(s, e));
			}else{
				s = temp+1;
				e = result.indexOf("</div", s);
				//System.out.println(result.substring(s, e));
				rank_deck_temp.add(result.substring(s, e));
			}
			//System.out.println("�ȳ�ȳ�ȳ�");
			
		}
		
		start = 0;
		
		int t = 0;
		
    	while(true){
    		s = result.indexOf("<li rel=\"", start);
    		start = s+1;
    		e = result.indexOf("\">", s);
    		if (s==-1) break;
    	//	System.out.println("http://hs.inven.co.kr/dataninfo/deck/view.php?idx=" + result.substring(s+9, e));
    		rank_deck_url.add(result.substring(s+9, e));
    		
    		s = result.indexOf("http://static.inven.co.kr/image_2011/hs/fonticon/fonticon_job", s);
    		e = "http://static.inven.co.kr/image_2011/hs/fonticon/fonticon_job".length();
    		System.out.println(rank_deck_temp.get(t));
    		System.out.println(result.substring(s+e, s+e+1));
    		if (result.substring(s+e, s+e+1).equals("1")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRowarrior");
    			//warrior
    		}else if (result.substring(s+e, s+e+1).equals("2")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRoshaman");
    			//shaman
    		}else if (result.substring(s+e, s+e+1).equals("3")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRorogue");
    			//rouge
    		}else if (result.substring(s+e, s+e+1).equals("4")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRopaladin");
    			//paladin
    		}else if (result.substring(s+e, s+e+1).equals("5")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRohunter");
    			//hunter
    		}else if (result.substring(s+e, s+e+1).equals("6")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRodruid");
    			//druid
    		}else if (result.substring(s+e, s+e+1).equals("7")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRowarlock");
    			//warlock
    		}else if (result.substring(s+e, s+e+1).equals("8")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRomage");
    			//mage
    		}else if (result.substring(s+e, s+e+1).equals("9")){
    			rank_deck.add(rank_deck_temp.get(t)+"*heRopriest");
    			//priest
    		}
    		t++;
    	}
		
    	return result;
	}

    
	// Sets the Bitmap returned by doInBackground
    @Override
    protected void onPostExecute(String result) {
    	
    }

}



