package com.example.hs;

//http://wow.zamimg.com/images/hearthstone/cards/enus/medium/CS1_113.png

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends ArrayAdapter<JSONObject>{
	private Context mContext;
	private ArrayList<JSONObject> list;
	private LayoutInflater mInflater;
	final int scaleImageX=85;
	final int scaleImageY=123;

	public ArrayList<JSONObject> getList() {
		return list;
	}

	public void setList(ArrayList<JSONObject> list) {
		this.list = list;
	}
	
	private LayoutInflater inflater;
	

	public ImageAdapter(Context context, ArrayList<JSONObject> list){
		super(context, 0, list);
		this.mContext = context;
		this.list = list;
		this.inflater = LayoutInflater.from(context);
		//Log.i("aa","aa");
	}
	//Override
	/*
	public int getCount(){
		return imageids.length;
	}
	public Object getItem(int position){
		return imageids[position];
	}
	public long getItemId(int position) {
		return 0;
	}*/
	

	public View getView(int position, View convertView, ViewGroup parent){
		ImageView imageView = new ImageView(mContext);
		GetXMLTask task = new GetXMLTask( mContext, imageView);
        try {
			task.execute(new String[] { getItem(position).getString("image_url") });
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        
        
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	
        imageView.setLayoutParams(new GridView.LayoutParams(
                (int) mContext.getResources().getDimension(R.dimen.width),                                                                                   
                (int) mContext.getResources().getDimension(R.dimen.height)));
		return imageView;

	
		
	}
}

