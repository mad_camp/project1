package com.example.hs;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;

public class Fullimage_Activity extends Activity {

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, 
		                 WindowManager.LayoutParams.FLAG_BLUR_BEHIND);*/
		
		WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
		layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		layoutParams.dimAmount = 0.8f; // ������ 0 ~ 1
		getWindow().setAttributes(layoutParams);
		
		setContentView(R.layout.full_image);
		
		
		Intent i = getIntent();
		
		String url = i.getExtras().getString("id");
		
		
		ImageView imageView = (ImageView) findViewById(R.id.full);
		GetXMLTask task = new GetXMLTask(getApplicationContext(), imageView);
		task.execute(new String[] { url });

	}
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			finish();
			return true;
			}
		return false;
		}

}
