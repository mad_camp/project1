package com.example.hs;

//http://wow.zamimg.com/images/hearthstone/cards/enus/medium/CS1_113.png

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.AssetManager.AssetInputStream;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
	private int REQUEST_CODE = 1;
	private ArrayList<JSONObject> items;
	private CardAdapter adapter;
	private ImageAdapter iadapter;
	private DeckListAdapter dadapter;
	private WebAdapter wadapter;
	
	private ListView listView;
	private ListView listView2;
	private ListView listView3;
	private ListView listView4;
	
	private JSONArray cards;
	private ArrayList<JSONObject> deck;
	private ArrayAdapter<String> adspin;
	
	private Spinner spinner;
	private Spinner spinner2;
	private Spinner spinner3;
	
	private ArrayList<String> deckList;
	
	private ArrayList<String> rank_deckList;
	private ArrayList<String> rank_deckURL;
	private ArrayList<String> card_info;
	
	private String current_deck;
	private String enemy;
	int current_page;
	
	
	
	private int heroFilterPosition;
	private int manaFilterPosition;
	
	
	private boolean changeinven=false;
	/////////////////////////////////////
	
	/////////////////////////////////////
	
	
	
	private ArrayList<String> heroes;
	private ArrayAdapter<String> sadapter;
	
	DB_score db_score;
	SQLiteDatabase db;
	
	static class JSONCmp implements Comparator<JSONObject>{

		@Override
		public int compare(JSONObject lhs, JSONObject rhs) {
			try {
				if (lhs.getInt("mana") < rhs.getInt("mana")){
					return -1;
				}else if (lhs.getInt("mana") > rhs.getInt("mana")){
					return 1;
				}else{
					if (lhs.getString("name").compareTo(rhs.getString("name")) > 0){
						return 1;
					}else if (lhs.getString("name").compareTo(rhs.getString("name")) < 0){
						return -1;
					}
					return 0;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return 0;
		}
		
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
    	///requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        enemy = null;
        current_deck=null;
        current_page=1;
        
        heroes = new ArrayList<String>();
        heroes.add("druid");
        heroes.add("hunter");
        heroes.add("mage");
        heroes.add("paladin");
        heroes.add("priest");
        heroes.add("rogue");
        heroes.add("shaman");
        heroes.add("warlock");
        heroes.add("warrior");
        
        heroFilterPosition = 0;
        manaFilterPosition = 0;
        
        //// db create /////
        db_score = new DB_score(this);
        ////////////////////
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setPrompt("Select Enemy Hero.");
        
        ArrayAdapter<?> adaption = ArrayAdapter.createFromResource(
        		this, R.array.Hero, R.layout.item_spinner);
        adaption.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaption);
        
        
        spinner2 = (Spinner) findViewById(R.id.mana_spinner);
        spinner2.setPrompt("Select mana cost.");
        
        adaption = ArrayAdapter.createFromResource(
        		this, R.array.Mana, R.layout.item_spinner);
        adaption.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adaption);
        
        
        spinner3 = (Spinner) findViewById(R.id.hero_spinner);
        spinner3.setPrompt("Select Enemy Hero.");
        
        adaption = ArrayAdapter.createFromResource(
        		this, R.array.Hero2, R.layout.item_spinner);
        adaption.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adaption);
        
        spinner.setOnItemSelectedListener(new OnItemSelectedListener(){
        	public void onItemSelected(AdapterView<?> parent,View view, int position, long id){
        			ImageView image = (ImageView) findViewById(R.id.Enemy);
        			ImageEnemy(position, image);
        			Integer t = new Integer(position);
        			enemy = "h"+ t.toString();
        			if (position == 0) enemy = null;
        	}
        	public void onNothingSelected(AdapterView<?> parent){
        		;
        	}
        });
        
        spinner2.setOnItemSelectedListener(new OnItemSelectedListener(){
        	public void onItemSelected(AdapterView<?> parent,View view, int position, long id){
        		manaFilterPosition = position;
        		items.clear();
        		filter(manaFilterPosition, heroFilterPosition);
        		adapter.notifyDataSetChanged();
        	}
        	public void onNothingSelected(AdapterView<?> parent){
        		;
        	}
        });
        
        spinner3.setOnItemSelectedListener(new OnItemSelectedListener(){
        	public void onItemSelected(AdapterView<?> parent,View view, int position, long id){
        		heroFilterPosition = position;
        		items.clear();
        		filter(manaFilterPosition, heroFilterPosition);
        		adapter.notifyDataSetChanged();
        	}
        	public void onNothingSelected(AdapterView<?> parent){
        		;
        	}
        });
        
        
        
        AssetManager assetManager = getResources().getAssets();
        try {
			AssetInputStream ais = (AssetInputStream)assetManager.open("card.json");
			BufferedReader br = new BufferedReader(new InputStreamReader(ais));
			StringBuilder sb = new StringBuilder();
			int bufferSize = 1024*1024;
			char readBuf[] = new char[bufferSize];
			int resultSize = 0;
			
			while((resultSize = br.read(readBuf)) != -1){
				if (resultSize == bufferSize){
					sb.append(readBuf);
				}else{
					for (int i=0; i<resultSize; i++){
						sb.append(readBuf[i]);
					}
				}
			}
			String jString = sb.toString();
			JSONObject jo = new JSONObject(jString);
			cards = (JSONArray)jo.getJSONArray("cards");
			listView = (ListView)findViewById(R.id.list);
			items = new ArrayList<JSONObject>();
			deck = new ArrayList<JSONObject>();
			
			for (int i=0; i<cards.length(); i++){
				items.add(cards.getJSONObject(i));
			}
			
			Collections.sort(items, new JSONCmp());
			
			adapter = new CardAdapter(this, items);
	        listView.setAdapter(adapter);
	        
	        listView.setOnItemClickListener(new OnItemClickListener(){
	        	public void onItemClick(AdapterView<?> parent, View v, int position, long id){
	        		Intent i = new Intent(getApplicationContext(), Fullimage_Activity.class);
	        		try {
						i.putExtra("id", items.get(position).getString("image_url"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
	        		startActivity(i);
	        	}
	        });

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
        /////////////////
        deckList = new ArrayList<String>();
        File file = new File("data/data/com.example.hs/files/");
        if (file != null){
	        File[] fileList = file.listFiles();

	        if (fileList != null){
		        for (int i=0; i<fileList.length; i++){
		        	String temp = fileList[i].getName();
		        	deckList.add(temp.substring(0, temp.length()-5));
		        }
	        }
        }
       
        
        listView2 = (ListView)findViewById(R.id.list2);
        dadapter = new DeckListAdapter(getApplicationContext(), deckList, db_score);
        listView2.setAdapter(dadapter);
        listView2.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				current_deck = deckList.get(arg2);
				
				TextView name = (TextView)findViewById(R.id.deck_name);
				name.setTypeface(Typeface.createFromAsset(getAssets(), "Hun_pro_R.ttf"));
				name.setText(current_deck);
				
				db = db_score.getWritableDatabase();
				Cursor c = db.rawQuery("SELECT * FROM db_deck WHERE name='"+current_deck+ "'", null);
            	c.moveToFirst();
            	ImageView image = (ImageView) findViewById(R.id.Mine);
            	String hero = c.getString(2);
            	int position = 0;
            	if (hero.equals("druid")) position=1;
            	else if (hero.equals("hunter")) position=2;
            	else if (hero.equals("mage")) position=3;
            	else if (hero.equals("paladin")) position=4;
            	else if (hero.equals("priest")) position=5;
            	else if (hero.equals("rogue")) position=6;
            	else if (hero.equals("shaman")) position=7;
            	else if (hero.equals("warlock")) position=8;
            	else if (hero.equals("warrior")) position=9;
    			ImageEnemy(position, image);
            	
    			sadapter = new ScoreAdapter(getApplicationContext(), heroes, db_score, current_deck);
    	        listView3.setAdapter(sadapter);
            	sadapter.notifyDataSetChanged();
				db.close();
				
			}
        });
        listView2.setOnItemLongClickListener(new OnItemLongClickListener(){
        	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3){
        		AlertDialog.Builder alertDlg = new AlertDialog.Builder(MainActivity.this);
                alertDlg.setTitle(R.string.alert_title_question3);
                alertDlg.setPositiveButton( R.string.button_1, new DialogInterface.OnClickListener()
                {
                     @Override
                     public void onClick( DialogInterface dialog, int which ) 
                     {
                    	FileInputStream ais;
						try {
							ais = new FileInputStream("data/data/com.example.hs/files/" + deckList.get(arg2) + ".json");
							BufferedReader br = new BufferedReader(new InputStreamReader(ais));
	             			StringBuilder sb = new StringBuilder();
	             			int bufferSize = 1024*1024;
	             			char readBuf[] = new char[bufferSize];
	             			int resultSize = 0;
	             			
	             			while((resultSize = br.read(readBuf)) != -1){
	             				if (resultSize == bufferSize){
	             					sb.append(readBuf);
	             				}else{
	             					for (int i=0; i<resultSize; i++){
	             						sb.append(readBuf[i]);
	             					}
	             				}
	             			}
	             			String jString = sb.toString();
	             			JSONObject jo = new JSONObject(jString);
	             			deck.clear();
	             			JSONArray temp = jo.getJSONArray("deck");
	             			for (int i=0; i<temp.length(); i++){
	            				deck.add(temp.getJSONObject(i));
	            			}
	   
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (JSONException e) {
							e.printStackTrace();
						}
             			
             			
                        iadapter.notifyDataSetChanged();   
                       
                        dialog.dismiss();  
                     }
                });
                alertDlg.setNegativeButton( R.string.button_2, new DialogInterface.OnClickListener()
                {
                     @Override
                     public void onClick( DialogInterface dialog, int which ) {
                    	 
                    	 File file = new File("data/data/com.example.hs/files/" + deckList.get(arg2) + ".json");
                    	 file.delete();
                    	 db = db_score.getWritableDatabase();
                     	 db.execSQL("DELETE FROM db_deck WHERE name='" + deckList.get(arg2) +"'");	
                         db.close();
                    	 deckList.remove(arg2);
                    	 dadapter.notifyDataSetChanged();
                    	 current_deck = null;
                         dialog.dismiss();  // AlertDialog를 닫는다.
                     }
                });
                alertDlg.show();
    			return true;
        	}
        });
        
        listView3 = (ListView)findViewById(R.id.list3);
      //  ScoreHero_Adapter adpater3 = neFw ScoreHero_Adapter(getApplicationContext(),); // Hero별 승무패 담은 어레이
        
        /////////////////
        
        
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
        
        //Tab1
        TabSpec spec1 = tabHost.newTabSpec("Tab1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("",getResources().getDrawable(R.drawable.icon_data));
        tabHost.addTab(spec1);
        
      //Tab2
        TabSpec spec2 = tabHost.newTabSpec("Tab2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("",getResources().getDrawable(R.drawable.icon_image));
        tabHost.addTab(spec2);
      //Tab3
        TabSpec spec3 = tabHost.newTabSpec("Tab3");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("",getResources().getDrawable(R.drawable.icon_decklist));
        tabHost.addTab(spec3);
        
      //Tab4
        TabSpec spec4 = tabHost.newTabSpec("Tab4");
        spec4.setContent(R.id.tab4);
        spec4.setIndicator("",getResources().getDrawable(R.drawable.icon_web));
        tabHost.addTab(spec4);
        
        
        tabHost.getTabWidget().getChildAt(0).getLayoutParams().height=80;
        tabHost.getTabWidget().getChildAt(1).getLayoutParams().height=80;
        tabHost.getTabWidget().getChildAt(2).getLayoutParams().height=80;
        tabHost.getTabWidget().getChildAt(3).getLayoutParams().height=80;
        
        tabHost.setCurrentTab(0);
        
        GridView gridview = (GridView) findViewById(R.id.grid_View);
        iadapter = new ImageAdapter(this, deck);
        gridview.setAdapter(iadapter);
        
        gridview.setOnItemClickListener(new OnItemClickListener(){
        	public void onItemClick(AdapterView<?> parent, View v, int position, long id){
        		Intent i = new Intent(getApplicationContext(), Fullimage_Activity.class);
        		try {
					i.putExtra("id", deck.get(position).getString("image_url"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
        		startActivity(i);
        	}
        });
        gridview.setOnItemLongClickListener(new OnItemLongClickListener(){
        	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3){
        		AlertDialog.Builder alertDlg = new AlertDialog.Builder(arg1.getContext());
                alertDlg.setTitle(R.string.alert_msg_del);
                alertDlg.setPositiveButton( R.string.button_yes, new DialogInterface.OnClickListener()
                {
                     @Override
                     public void onClick( DialogInterface dialog, int which ) 
                     {
                    	 if (deck.size() > 0){
                    		try {
                    			 deck.remove(arg2);
                    			 Toast toast=Toast.makeText(getApplicationContext(), R.string.card_del, Toast.LENGTH_SHORT);
     							 toast.setGravity(Gravity.BOTTOM, 0, 0);
     							 toast.show();
    						}
                    		finally{ }
                    	 }
                         iadapter.notifyDataSetChanged();                     
                         dialog.dismiss();  
                     }
                });
                alertDlg.setNegativeButton( R.string.button_no, new DialogInterface.OnClickListener()
                {
                     @Override
                     public void onClick( DialogInterface dialog, int which ) {
                         dialog.dismiss();  // AlertDialog를 닫는다.
                     }
                });
                alertDlg.show();
    			return true;
        	}
        });
        
        listView.setOnItemClickListener(new OnItemClickListener(){
        	public void onItemClick(AdapterView<?> parent, View v, int position, long id){
        		Intent i = new Intent(getApplicationContext(), Fullimage_Activity.class);
        		try {
					i.putExtra("id", items.get(position).getString("image_url"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
        		startActivity(i);
        	}
        });
        
        listView.setOnItemLongClickListener( new ListViewItemLongClickListener() );
        
        Button clear_button = (Button)findViewById(R.id.clear_button);
        clear_button.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
        		alert.setMessage(R.string.clear_question);
        		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// deck delete
						int size=deck.size();
						if (size==0)
						{
							Toast toast=Toast.makeText(getApplicationContext(), R.string.empty_deck, Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.BOTTOM, 0, 0);
							toast.show();
							dialog.dismiss();
							return;
						}
						for(int i=0;i<size;i++){
							deck.remove(0);
						}
						dadapter.notifyDataSetChanged();
                    	iadapter.notifyDataSetChanged();
                    	dialog.dismiss();
                    	Toast toast=Toast.makeText(getApplicationContext(), R.string.cleared, Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.BOTTOM, 0, 0);
						toast.show();
					
						
					}
				});
        		alert.setNegativeButton("No", new DialogInterface.OnClickListener(){
        			
        			public void onClick(DialogInterface dialog, int which){
        				dialog.cancel();
        			}
        		});
        		AlertDialog dialog = alert.create();
        		dialog.show();
        		
        	}
        });
        
        
        Button button = (Button)findViewById(R.id.add_button);
        button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alertDlg = new AlertDialog.Builder(v.getContext());
                alertDlg.setTitle(R.string.alert_title_question2);
                final EditText input = new EditText(v.getContext());
                alertDlg.setView(input);
                
                alertDlg.setPositiveButton( R.string.button_yes, new DialogInterface.OnClickListener()
                {
                     @Override
                     public void onClick( DialogInterface dialog, int which ) 
                     {
                    	 for (int i=0; i<deckList.size(); i++){
                    		 if (deckList.get(i).equals(input.getText().toString())){
                    			 return;
                    		 }
                    	 }
                    	 String hero = create_file(input.getText().toString(), dialog, false);
                    	
                    	if (hero==null) return;
                    	db = db_score.getWritableDatabase();
                    	db.execSQL("INSERT INTO db_deck VALUES (null, '" + input.getText().toString() + "' ,"
                    										+"'"+hero+"', "
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0,"
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0, "
                    										+" 0, 0, 0)");
                    	db.close();
                    	
                    	dadapter.notifyDataSetChanged();
                    	iadapter.notifyDataSetChanged();
                        dialog.dismiss();  
                        
                     }
                });
                alertDlg.setNegativeButton( R.string.button_no, new DialogInterface.OnClickListener()
                {
                     @Override
                     public void onClick( DialogInterface dialog, int which ) {
                         dialog.dismiss();
                     }
                });
                alertDlg.show();
    			return;
				
			}
		});
        
        Button button_win = (Button)findViewById(R.id.win);
        button_win.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				if (current_deck != null && enemy != null){
					db = db_score.getWritableDatabase();
                	db.execSQL("UPDATE db_deck set " + enemy +  "w=(" + enemy + "w)+1 where " + "name='" + current_deck + "'");
                	db.close();
                	dadapter.notifyDataSetChanged();
                	sadapter.notifyDataSetChanged();
                	enemy = null;
                	spinner.setSelection(0);
				}
			}
        });
        
        Button button_draw = (Button)findViewById(R.id.draw);
        button_draw.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (current_deck != null && enemy != null){
					db = db_score.getWritableDatabase();
					db.execSQL("UPDATE db_deck set " + enemy +  "d=(" + enemy + "d)+1 where " + "name='" + current_deck + "'");
                	db.close();
                	dadapter.notifyDataSetChanged();
                	sadapter.notifyDataSetChanged();
                	enemy = null;
                	spinner.setSelection(0);
				}
			}
        });
        
        Button button_lose = (Button)findViewById(R.id.lose);
        button_lose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (current_deck != null && enemy != null){
					db = db_score.getWritableDatabase();
					db.execSQL("UPDATE db_deck set " + enemy +  "l=(" + enemy + "l)+1 where " + "name='" + current_deck + "'");
                	db.close();
                	dadapter.notifyDataSetChanged();
                	sadapter.notifyDataSetChanged();
                	enemy = null;
                	spinner.setSelection(0);
				}
			}
        });
        
        /////////
        listView3 = (ListView) findViewById(R.id.list3);
        sadapter = new ScoreAdapter(getApplicationContext(), heroes, db_score, current_deck);
        listView3.setAdapter(sadapter);
        
        /////////
        
        listView3.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				spinner.setSelection(position+1);
				
			}
        	
        });
        
        ///modify button///
        Button button_modify = (Button)findViewById(R.id.modify_button);
        button_modify.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (current_deck != null){
					AlertDialog.Builder alertDlg = new AlertDialog.Builder(v.getContext());
	                alertDlg.setTitle(R.string.modify_question);
	                alertDlg.setPositiveButton( R.string.button_yes, new DialogInterface.OnClickListener()
	                {
	                     @Override
	                     public void onClick( DialogInterface dialog, int which ) 
	                     {
	                    	String hero;
	                    	if(current_deck!=null)
	                    		hero = create_file(current_deck, dialog, true);
	                    	else{
	                    		dialog.dismiss();
	                    		return;
	                    	}
	                    	if (hero == null) return;
	                    	
	                    	db = db_score.getWritableDatabase();
	                    	db.execSQL("UPDATE db_deck set hero='" + hero + "' where " + "name='" + current_deck + "'");
	                    	db.close();
	                    	
	                    	dadapter.notifyDataSetChanged();
	                    	iadapter.notifyDataSetChanged();
	                        dialog.dismiss();  
	                        
	                     }
	                });
	                alertDlg.setNegativeButton( R.string.button_no, new DialogInterface.OnClickListener()
	                {
	                     @Override
	                     public void onClick( DialogInterface dialog, int which ) {
	                         dialog.dismiss();
	                     }
	                });
	                if(current_deck!=null) alertDlg.setMessage(current_deck);
	                alertDlg.show();
	    			return;
					
				}
			}
        });
        /////////////////// 
        
        //tab4
        
        //prev button click event
        Button prev = (Button)findViewById(R.id.prev);
        prev.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		if (current_page != 1){
        			current_page--;
        			
        			
        			Integer c = new Integer(current_page);
        			EditText edit=(EditText)findViewById(R.id.page_edit);
            		edit.setText(c.toString());
            		rank_deckList.clear();
            		rank_deckURL.clear();
            		
            		String URL ;
            		if (changeinven==true){
            			GetXMLTask4 task = new GetXMLTask4(getApplicationContext(), rank_deckList, rank_deckURL);
            			URL = "http://m.inven.co.kr/hs/deck/?confirm=1&page="+c.toString();
            			try {
            				task.execute(new String[] {URL}).get();
            			} catch (InterruptedException e) {
            				e.printStackTrace();
            			} catch (ExecutionException e) {
            				e.printStackTrace();
            			}
            		}else{
            			GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
            			URL = "http://www.hearthpwn.com/decks?filter-deck-tag=2&page="+c.toString()+"&sort=-rating";
            			try {
            				task.execute(new String[] {URL}).get();
            			} catch (InterruptedException e) {
            				e.printStackTrace();
            			} catch (ExecutionException e) {
            				e.printStackTrace();
            			}
            		}
        			
        			wadapter.notifyDataSetChanged();
        		}
        	}
        });
        
        //next button click event
        Button next = (Button)findViewById(R.id.next);
        next.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		if (current_page < 1000){
        			current_page++;
        			//GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
        			
        			if (current_page>=6 && changeinven){
        				current_page=5;
        			}
        			
        			Integer c = new Integer(current_page);
        			EditText edit=(EditText)findViewById(R.id.page_edit);
            		edit.setText(c.toString());	
            		
            		rank_deckList.clear();
            		rank_deckURL.clear();
            		String URL;
            		
            		if (changeinven==true){
            			
            			GetXMLTask4 task = new GetXMLTask4(getApplicationContext(), rank_deckList, rank_deckURL);
            			URL = "http://m.inven.co.kr/hs/deck/?confirm=1&page="+c.toString();
            			try {
            				task.execute(new String[] {URL}).get();
            			} catch (InterruptedException e) {
            				e.printStackTrace();
            			} catch (ExecutionException e) {
            				e.printStackTrace();
            			}
            		}else{
            			GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
            			URL = "http://www.hearthpwn.com/decks?filter-deck-tag=2&page="+c.toString()+"&sort=-rating";
            			try {
            				task.execute(new String[] {URL}).get();
            			} catch (InterruptedException e) {
            				e.printStackTrace();
            			} catch (ExecutionException e) {
            				e.printStackTrace();
            			}
            		}
        			wadapter.notifyDataSetChanged();
        		}
        	}
        });
        
        //move button click event        
        Button page_move =(Button)findViewById(R.id.move);
        page_move.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		String page=null;
        		EditText edit=(EditText)findViewById(R.id.page_edit);
        		page=edit.getText().toString();
        		if (page.length() == 0) return;
        		
        		//System.out.println("buttonok");
        		//EditText edit=(EditText)findViewById(R.id.page_edit);
        		//EditText edit=(EditText)findViewById(R.id.page_edit);
        		
        		//page 가지고 새로 view 띄워주는 작업.
        		
        		
        		if (Integer.parseInt(page)>0 && Integer.parseInt(page) < 1001){
        			current_page = Integer.parseInt(page);
        			//GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
        			if (current_page>=6 && changeinven){
        				current_page=5;
        			}
        			rank_deckList.clear();
            		rank_deckURL.clear();
            		
            		Integer c = new Integer(current_page);
            		String URL;
            		if (changeinven==true){
            			GetXMLTask4 task = new GetXMLTask4(getApplicationContext(), rank_deckList, rank_deckURL);
            			URL = "http://m.inven.co.kr/hs/deck/?confirm=1&page="+c.toString();
            			try {
            				task.execute(new String[] {URL}).get();
            			} catch (InterruptedException e) {
            				e.printStackTrace();
            			} catch (ExecutionException e) {
            				e.printStackTrace();
            			}
            		}else{
            			GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
            			URL = "http://www.hearthpwn.com/decks?filter-deck-tag=2&page="+c.toString()+"&sort=-rating";
            			try {
            				task.execute(new String[] {URL}).get();
            			} catch (InterruptedException e) {
            				e.printStackTrace();
            			} catch (ExecutionException e) {
            				e.printStackTrace();
            			}
            		}
        			wadapter.notifyDataSetChanged();
        		}
        		
        	}
        });
       
        Button reset = (Button)findViewById(R.id.reset_button);
        reset.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		clearApplicationCache(null);
        	}
        });
        
        
        
        /////tab4//////
        rank_deckList = new ArrayList<String>();
        rank_deckURL = new ArrayList<String>();
        card_info = new ArrayList<String>();
        
       
        GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
		try {
			Integer c = new Integer(current_page);
			task.execute(new String[] {"http://www.hearthpwn.com/decks?filter-deck-tag=2&page="+c.toString()+"&sort=-rating" }).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
        listView4 = (ListView) findViewById(R.id.list4);
        wadapter = new WebAdapter(getApplicationContext(), rank_deckList);
        listView4.setAdapter(wadapter);
        listView4.setOnItemLongClickListener( new ListView4ItemLongClickListener() );
        
    
        
        //////////////
        //////////to inven button/////////////////
        /////////////
        final Button inven = (Button)findViewById(R.id.inven);
        inven.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(changeinven==false){
					changeinven=true;
					
					current_page = 1;
					inven.setBackgroundResource(R.drawable.rightarrow);
					GetXMLTask4 task = new GetXMLTask4(getApplicationContext(), rank_deckList, rank_deckURL);
        			Integer c = new Integer(current_page);
        			EditText edit=(EditText)findViewById(R.id.page_edit);
            		edit.setText(c.toString());
            		rank_deckList.clear();
            		rank_deckURL.clear();
            		
            		String URL ;
            		if (changeinven==true)
            			URL = "http://m.inven.co.kr/hs/deck/?confirm=1&page="+c.toString();
            		else
            			URL = "http://www.hearthpwn.com/decks?filter-deck-tag=2&page="+c.toString()+"&sort=-rating";
            		
        			try {
        				task.execute(new String[] {URL}).get();
        			} catch (InterruptedException e) {
        				e.printStackTrace();
        			} catch (ExecutionException e) {
        				e.printStackTrace();
        			}
        			//System.out.println("--안녕==="+ rank_deckList.size());
        			//System.out.println("======"+ rank_deckURL.size());
        			
        			wadapter.notifyDataSetChanged();
					
				}
				else{
					changeinven=false;
					current_page = 1;
					inven.setBackgroundResource(R.drawable.rightarrow);
					GetXMLTask2 task = new GetXMLTask2(getApplicationContext(), rank_deckList, rank_deckURL);
        			Integer c = new Integer(current_page);
        			EditText edit=(EditText)findViewById(R.id.page_edit);
            		edit.setText(c.toString());
            		rank_deckList.clear();
            		rank_deckURL.clear();
            		
            		String URL ;
            		if (changeinven==true)
            			URL = "http://m.inven.co.kr/hs/deck/?confirm=1&page="+c.toString();
            		else
            			URL = "http://www.hearthpwn.com/decks?filter-deck-tag=2&page="+c.toString()+"&sort=-rating";
            		
        			try {
        				task.execute(new String[] {URL}).get();
        			} catch (InterruptedException e) {
        				e.printStackTrace();
        			} catch (ExecutionException e) {
        				e.printStackTrace();
        			}
        			wadapter.notifyDataSetChanged();
					
					
					inven.setBackgroundResource(R.drawable.leftarrow);
					
					
				}
			}
        	
        });
        
        
    }
    
    //tab1 ListView Long_Click event
    private class ListViewItemLongClickListener implements AdapterView.OnItemLongClickListener
    {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
			AlertDialog.Builder alertDlg = new AlertDialog.Builder(arg1.getContext());
            alertDlg.setTitle(R.string.alert_title_question);

            // '예' 버튼이 클릭되면
            alertDlg.setPositiveButton( R.string.button_yes, new DialogInterface.OnClickListener()
            {
                 @Override
                 public void onClick( DialogInterface dialog, int which ) 
                 {
                	 try {
	                	 if (deck.size() < 30){
	                		JSONObject card = items.get(arg2);
	                		int cnt, i;
	                		boolean canadd = false;
	                		cnt = 0;
	                		for (i=0; i<deck.size(); i++){
	                			if (deck.get(i) == card){
	                				cnt++;
	                			}
	                		}
	                		if (cnt == 0){
	                			canadd=true;
	                		} else if (cnt==1 && !card.getString("quality").equals("legendary")){
								canadd=true;	
							}
							
							if (!canadd){
								Toast toast=Toast.makeText(getApplicationContext(), R.string.alert_card, Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.BOTTOM, 0, 0);
								toast.show();
								dialog.dismiss();  
								return;
							}
							deck.add(card);
							Collections.sort(deck, new JSONCmp());
		                    iadapter.notifyDataSetChanged();
		                    dialog.dismiss();
		                    Toast toast=Toast.makeText(getApplicationContext(), R.string.card_add, Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.BOTTOM, 0, 0);
							toast.show();
	                	 } else {
	                		 Toast toast=Toast.makeText(getApplicationContext(), R.string.alert_maxcard, Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.BOTTOM, 0, 0);
								toast.show();
								return;
	                	 }
                	 } catch (JSONException e) {
 						e.printStackTrace();
 					}
                }
            });
            
            // '아니오' 버튼이 클릭되면
            alertDlg.setNegativeButton( R.string.button_no, new DialogInterface.OnClickListener()
            {
                 @Override
                 public void onClick( DialogInterface dialog, int which ) {
                     dialog.dismiss();  // AlertDialog를 닫는다.
                 }
            });
            
			try {
				alertDlg.setMessage(items.get(arg2).getString("name"));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
            alertDlg.show();
			return true;
		}
    	
    }
   
    //for set enemy_image in tab3
    private void ImageEnemy(int position, ImageView image){
    	switch(position){
    	case 1 : image.setImageResource(R.drawable.hero_1);
    		break;
    	case 2 : image.setImageResource(R.drawable.hero_2);
    		break;
    	case 3 : image.setImageResource(R.drawable.hero_3);
    		break;
    	case 4 : image.setImageResource(R.drawable.hero_4);
    		break;
    	case 5 : image.setImageResource(R.drawable.hero_5);
    		break;
    	case 6 : image.setImageResource(R.drawable.hero_6);
    		break;
    	case 7 : image.setImageResource(R.drawable.hero_7);
    		break; 
    	case 8 : image.setImageResource(R.drawable.hero_8);
    		break;
    	case 9 : image.setImageResource(R.drawable.hero_9);
    		break;
    	case 0 : image.setImageDrawable(null);
    		break;
    	}
    }
    
    //for filtering cards
    private void filter(int mpos, int hpos){
    	String hero;
    	int mana;
    	mana = -1;
    	hero = null;
    	
    	//System.out.println(mpos);
    	//System.out.println(hpos);
    	
    	try {
	    	if (mpos == 0 && hpos == 0){
	    		mana = -1;
	        	hero = null;
	    	}else if (mpos == 0){
	    		if (hpos != 10)
	    			hero = heroes.get(hpos-1);
	    		else
	    			hero = "neutral";
	    	}else if (hpos == 0){
	    		mana = mpos-1;
	    	}else{
	    		if (hpos != 10)
	    			hero = heroes.get(hpos-1);
	    		else
	    			hero = "neutral";
	    		mana = mpos-1;
	    	}
	    	boolean m, h;
	    	m=h=false;
	    	
    		for (int i=0; i<cards.length(); i++){
    			m=h=false;
    			JSONObject temp = cards.getJSONObject(i);
    			if (mana == -1) m=true;
    			else if (mana < 9){
    				//System.out.println(temp.getInt("mana"));
    				//System.out.println(mana);
    				if (temp.getInt("mana") == mana) m = true;
    			}else{
    				if (temp.getInt("mana") >= mana) m = true;
    			}
    			
    			if (hero == null) h=true;
    			else if (hero.equals(temp.getString("hero"))){
    				h=true;
    			}
    			
    			if (m && h){
    				items.add(temp);
    			}
    		}
    	} catch (JSONException e) {
			e.printStackTrace();
		}
		Collections.sort(items, new JSONCmp());
    }
    
    private String create_file(String file_name, DialogInterface dialog, boolean modify){
    	JSONObject temp = new JSONObject();
    	JSONArray ja = new JSONArray();
    	String hero = null;
    	int l = deck.size();
    	for (int i=0; i<l; i++){
    		try {
				if (!deck.get(i).getString("hero").equals("neutral")){
					if(hero == null) hero = deck.get(i).getString("hero");
					else{
						if (!hero.equals(deck.get(i).getString("hero"))){
							Toast toast=Toast.makeText(getApplicationContext(), R.string.deck_error2, Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.BOTTOM, 0, 0);
							toast.show();
							dialog.dismiss();
							return null;
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
    		ja.put(deck.get(i));
    	}
    	deck.clear();
    	try {
			temp.put("deck", ja);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
    	
    	
    	FileOutputStream f;
    	try {
			f = openFileOutput(file_name + ".json", getApplicationContext().MODE_PRIVATE);
			if (!modify) deckList.add(0, file_name);
			f.write(temp.toString().getBytes());
			f.flush();
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	
    	return hero;
    }
    
    private class ListView4ItemLongClickListener implements AdapterView.OnItemLongClickListener
    {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
			AlertDialog.Builder alertDlg = new AlertDialog.Builder(MainActivity.this);
            alertDlg.setTitle(R.string.web_question);

            // '예' 버튼이 클릭되면
            alertDlg.setPositiveButton( R.string.button_1, new DialogInterface.OnClickListener()
            {
                 @Override
	            public void onClick( DialogInterface dialog, int which ) 
	            {
                	card_info.clear();
                	deck.clear();
                	
                	if (changeinven == false){
	                	GetXMLTask3 task2 = new GetXMLTask3(getApplicationContext(), card_info);
	                	System.out.println(rank_deckURL.get(arg2));
	         			try {
							task2.execute(new String[] {"http://www.hearthpwn.com/" + rank_deckURL.get(arg2) }).get();
							
							for (int j=0; j<30; j++){
								for (int i=0; i<cards.length(); i++){
			        				JSONObject temp = cards.getJSONObject(i);
			        				if (temp.getString("name").equals(card_info.get(j))){
			        					deck.add(temp);
			        				}
			        			}
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						} catch (JSONException e) {
							e.printStackTrace();
						}
                	}else{
                		GetXMLTask5 task2 = new GetXMLTask5(getApplicationContext(), card_info);
	                	System.out.println(rank_deckURL.get(arg2));
	         			try {
							task2.execute(new String[] { rank_deckURL.get(arg2) }).get();
							
							for (int j=0; j<30; j++){
								System.out.println(card_info.get(j));
								String a = card_info.get(j).replaceAll("\\\\", "");
								for (int i=0; i<cards.length(); i++){
			        				JSONObject temp = cards.getJSONObject(i);
			        				String temp2 = temp.getString("name");
			        				
			        				temp2 = temp2.replaceAll(" ", "");
			        				if (temp2.equalsIgnoreCase(a)){
			        					deck.add(temp);
			        				}
			        			}
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						} catch (JSONException e) {
							e.printStackTrace();
						}
                	}
         			
         			Collections.sort(deck, new JSONCmp());
         			iadapter.notifyDataSetChanged();
         			
                	dialog.dismiss();
               }
            });
            
            // '아니오' 버튼이 클릭되면
            alertDlg.setNegativeButton( R.string.button_3, new DialogInterface.OnClickListener()
            {
                 @Override
                 public void onClick( DialogInterface dialog, int which ) {
                	 Intent i= new Intent(getApplicationContext(), Web_Activity.class);
                	 if (!changeinven){
                		 i.putExtra("weburl","http://www.hearthpwn.com/" + rank_deckURL.get(arg2));
                	 }else{
                		 i.putExtra("weburl", "http://hs.inven.co.kr/dataninfo/deck/view.php?idx=" + rank_deckURL.get(arg2));
                	 }
                	
                	 startActivity(i);
                     dialog.dismiss();  // AlertDialog를 닫는다.
                 }
            });
            alertDlg.show();
			return true;
		}
    	
    }
    private void clearApplicationCache(java.io.File dir){  
        if(dir==null)
            dir = getCacheDir();
        else;
        if(dir==null)
            return;
        else;
        java.io.File[] children = dir.listFiles();
        try{   
        	String prev = null;
        	for (int j=0; j<deck.size(); j++){
        		System.out.println("bbbb");
        		if (prev.equals(deck.get(j).getString("name"))){
        			System.out.println("aaaa");
        			continue;
        		}
        		 for(int i=0;i<children.length;i++){
                 	String temp = children[i].toString();
                 	int pos = temp.indexOf("cache/");
                 	pos+=6;
                 	String url = "http://wow.zamimg.com/images/hearthstone/cards/enus/medium/" + temp.substring(pos, temp.length());
	        		if (deck.get(j).getString("image_url").equals(url)){
	        			System.out.println(url);
	        			children[i].delete();
	        			ImageView imageView = new ImageView(getApplicationContext());
	            		GetXMLTask task = new GetXMLTask( getApplicationContext(), imageView);
	            		task.execute(new String[] { url });
	        		}
        		 }
        		 prev = deck.get(j).getString("name");
        	}
        	/*
            for(int i=0;i<children.length;i++){
            	String temp = children[i].toString();
            	int pos = temp.indexOf("cache/");
            	pos+=6;
            	String url = "http://wow.zamimg.com/images/hearthstone/cards/enus/medium/" + temp.substring(pos, temp.length());
            //	System.out.println(deck.get(0).getString("image_url"));
            //	System.out.println(url);
            	String prev = null;
            			
            	for (int j=0; j<deck.size(); j++){
            		if (deck.get(j).getString("image_url").equals(url)){
            			children[i].delete();
            			ImageView imageView = new ImageView(getApplicationContext());
                		GetXMLTask task = new GetXMLTask( getApplicationContext(), imageView);
                		task.execute(new String[] { url });
            		}
            	}
            	
            	
            }
            */
        }
        catch(Exception e){}
        
        System.out.println("aaaa");
       
        iadapter.notifyDataSetChanged();
    }
}

