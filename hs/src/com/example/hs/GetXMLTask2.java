package com.example.hs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.AssetManager.AssetInputStream;
import android.os.AsyncTask;


public class GetXMLTask2 extends AsyncTask<String, Void, String> {
	private Context context;
	private ArrayList<String> rank_deck;
	private ArrayList<String> rank_deck_url;
	

	public GetXMLTask2(Context context, ArrayList<String> rank_deck, ArrayList<String> rank_deck_url){
		this.context = context;
		this.rank_deck = rank_deck;
		this.rank_deck_url = rank_deck_url;
	}

    @Override
    protected String doInBackground(String... urls) {
    	String s=null;
        for (String url : urls) {
        	s = downHtml(url);
        }
        return s;
	}
	private String downHtml(String url) {
		StringBuilder html = new StringBuilder();

		try {
			URL u = new URL(url);
			HttpURLConnection conn= (HttpURLConnection)u.openConnection();
			if (conn!=null){
				//conn.setConnectTimeout(10000);
				conn.connect();
				InputStreamReader isr = new InputStreamReader(conn.getInputStream());
				BufferedReader br = new BufferedReader(isr);
				for(;;){
					String line = br.readLine();
					if (line == null) break;
					html.append(line+"\n");
				}
				br.close();

			}
			conn.disconnect();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		String result = html.toString();
		
		/*
		AssetManager assetManager = context.getResources().getAssets();
		AssetInputStream ais;
		c = new ArrayList<String>();
		dec = new ArrayList<String>();
		hex = new ArrayList<String>();
		entity = new ArrayList<String>();
		
		try {
			ais = (AssetInputStream)assetManager.open("html.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(ais));
			StringBuilder sb = new StringBuilder();
			for(;;){
				int i=0;
				
				String line = br.readLine();
				if (line == null) break;
				System.out.println(line);
				int pos = line.indexOf('&');
				if (pos == 0){
					c.add(null);
				}else{
					c.add(line.substring(0, pos));
				}
				
				int end = line.indexOf('&', pos+1);
				dec.add(line.substring(pos, end));
				
				pos = end;
				end = line.indexOf('&', pos+1);
				if (end==-1){
					hex.add(line.substring(pos, line.length()));
					entity.add(null);
				}else{
					hex.add(line.substring(pos, end));
					entity.add(line.substring(end, line.length()));
				}
				
				if (c.get(i)!= null) System.out.println(c.get(i));
				if (dec.get(i)!= null) System.out.println(dec.get(i));
				if (hex.get(i)!= null) System.out.println(hex.get(i));
				if (entity.get(i)!= null) System.out.println(entity.get(i));
				
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		int start = 0;
		
    	while(true){
			int pos = result.indexOf("<span class=\"tip\"", start);
			int pos2, end2;
			
			if (pos == -1) break;
			

			pos2 = result.indexOf("href=\"", pos);
			for (int i=0; i<2; i++){
	    		pos = result.indexOf(">", pos);
	    		pos++;
			}
			end2 = result.indexOf("class", pos2);
			pos2 += 6;
			end2 -= 2;
			
			rank_deck_url.add(result.substring(pos2, end2));
			
			
			
			int end = result.indexOf("<", pos);
			
			String temp2 = result.substring(result.indexOf("-", pos-15)+1, pos-2);
			
			String temp = result.substring(pos, end);
			if (temp.contains("&#x27;")){
				temp = temp.replaceAll("&#x27;", "'");
			}
			if (temp.contains("&quot;")){
				temp = temp.replaceAll("&quot;", "\"");
			}
			
			
			rank_deck.add(temp+"*heRo"+temp2);
			
			start = pos+1;
    	}
    	
    	return result;
	}

    
	// Sets the Bitmap returned by doInBackground
    @Override
    protected void onPostExecute(String result) {
    	
    }

}



