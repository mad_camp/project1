package com.example.hs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;


public class GetXMLTask3 extends AsyncTask<String, Void, String> {
	private Context context;
	private ArrayList<String> card_list;

	public GetXMLTask3(Context context, ArrayList<String> card_list){
		this.context = context;
		this.card_list = card_list;
	}

    @Override
    protected String doInBackground(String... urls) {
    	String s=null;
        for (String url : urls) {
        	s = downHtml(url);
        }
        return s;
	}
	private String downHtml(String url) {
		StringBuilder html = new StringBuilder();

		try {
			URL u = new URL(url);
			HttpURLConnection conn= (HttpURLConnection)u.openConnection();
			if (conn!=null){
				//conn.setConnectTimeout(10000);
				conn.connect();
				InputStreamReader isr = new InputStreamReader(conn.getInputStream());
				BufferedReader br = new BufferedReader(isr);
				for(;;){
					String line = br.readLine();
					if (line == null) break;
					html.append(line+"\n");
				}
				br.close();

			}
			conn.disconnect();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		String result = html.toString();
		int start = 0;
    	
    	//System.out.println("========");
    	while(true){
			int pos = result.indexOf("font-size: 110%", start);
			
			if (pos == -1) break;
			for (int i=0; i<2; i++){
	    		pos = result.indexOf(">", pos);
	    		pos++;
			}
			
			int end = result.indexOf("<", pos);
			String temp2 = result.substring(pos, end);
			if (temp2.contains("&#x27;")){
				temp2 = temp2.replaceAll("&#x27;", "'");
			}
			card_list.add(temp2);
			//System.out.println(temp2);
			if (result.charAt(end+13) == '2'){
				card_list.add(temp2);
			//	System.out.println(temp2);
			}
			start = pos+1;
    	}
    	
    	System.out.println(card_list.size());
    	
    	return result;
	}

    
	// Sets the Bitmap returned by doInBackground
    @Override
    protected void onPostExecute(String result) {
    	
    }

}



