package com.example.hs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class GetXMLTask extends AsyncTask<String, Void, Bitmap> {
	private ImageView imageView;
	private Context context;

	public GetXMLTask(Context context, ImageView imageView){
		this.context = context;
		this.imageView = imageView;
	}
	private String urlToFileFullPath(String url){
		return context.getCacheDir().getAbsolutePath() + url.substring(url.lastIndexOf("/"), url.length());
	}
	
    @Override
    protected Bitmap doInBackground(String... urls) {
        Bitmap map = null;
        for (String url : urls) {
        	String fileFullPath = urlToFileFullPath(url);
        //	String tempFilePath = fileFullPath+"_temp";
        	
        	if (new File(fileFullPath).exists()){
        		return BitmapFactory.decodeFile(fileFullPath);
        	}else{
        		map = downloadImage(url);
        		File newFile = new File(fileFullPath);
        		writeFile(map, newFile);
        	}
        }
        return map;
    }

    private void writeFile(Bitmap bmp, File f) {
		// TODO Auto-generated method stub
		FileOutputStream out = null;
		try{
			out = new FileOutputStream(f);
			bmp.compress(Bitmap.CompressFormat.PNG, 50, out);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if (out != null){
					out.close();
				}
			}catch(Exception ex){
			}
		}
	}
	// Sets the Bitmap returned by doInBackground
    @Override
    protected void onPostExecute(Bitmap result) {
    	
        imageView.setImageBitmap(result);
    }

    // Creates Bitmap from InputStream and returns it
    private Bitmap downloadImage(String url) {
        Bitmap bitmap = null;
        InputStream stream = null;
        try {
			stream = getHttpConnection(url);
			bitmap = BitmapFactory.decodeStream(stream);
            stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return bitmap;
    }

    // Makes HttpURLConnection and returns InputStream
    private InputStream getHttpConnection(String urlString) throws IOException {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();
        try {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.connect();
            stream = httpConnection.getInputStream();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stream;
    }
}