
package com.example.hs;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CardAdapter extends ArrayAdapter<JSONObject> {
	private Context context;
	private ArrayList<JSONObject> list;
	private LayoutInflater inflater;
	
	public CardAdapter(Context context, ArrayList<JSONObject> list){
		super(context, 0, list);
		this.context = context;
		this.list = list;
		this.inflater = LayoutInflater.from(context);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.list_row, null);
		}
		JSONObject item = getItem(position);
		
		TextView mana = (TextView)convertView.findViewById(R.id.mana);
		TextView name = (TextView)convertView.findViewById(R.id.name);
	//	TextView hero = (TextView)convertView.findViewById(R.id.hero);
		ImageView hero = (ImageView)convertView.findViewById(R.id.hero_icon);
		
		try {
			mana.setText(item.getString("mana"));
			name.setText(item.getString("name"));
			String quality = item.getString("quality");
			//System.out.println(quality);
			if (quality.equals("legendary")){
				name.setTextColor(context.getResources().getColor(R.color.color_gold));
			}else if(quality.equals("rare")){
				name.setTextColor(context.getResources().getColor(R.color.color_blue));
			}else if(quality.equals("common")){
				name.setTextColor(context.getResources().getColor(R.color.color_black));
			}else if(quality.equals("free")){
				name.setTextColor(context.getResources().getColor(R.color.color_gray));
			}else if(quality.equals("epic")){
				name.setTextColor(context.getResources().getColor(R.color.color_purple));
			}
			
		//	hero.setText(item.getString("hero"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{ 
			hero=heroimage(item.getString("hero"),hero);
				
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return convertView;
	}
	
	public ImageView heroimage(String hero,ImageView view){
		if(hero.equals("neutral")){
		//	System.out.println(hero);
			view.setImageResource(0);
		}else if(hero.equals("druid")){
			view.setImageResource(R.drawable.icon_1);
		}else if(hero.equals("hunter")){
			view.setImageResource(R.drawable.icon_2);
		}else if(hero.equals("mage")){
			view.setImageResource(R.drawable.icon_3);
		}else if(hero.equals("paladin")){
			view.setImageResource(R.drawable.icon_4);
		}else if(hero.equals("priest")){
			view.setImageResource(R.drawable.icon_5);
		}else if(hero.equals("rogue")){
			view.setImageResource(R.drawable.icon_6);
		}else if(hero.equals("shaman")){
			view.setImageResource(R.drawable.icon_7);
		}else if(hero.equals("warlock")){
			view.setImageResource(R.drawable.icon_8);
		}else if(hero.equals("warrior")){
			view.setImageResource(R.drawable.icon_9);
		}else{
			view.setImageResource(0);
		}
		return view;
	}
}
