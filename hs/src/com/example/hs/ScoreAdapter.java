package com.example.hs;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ScoreAdapter extends ArrayAdapter<String> {
	private Context context;
	private ArrayList<String> list;
	private LayoutInflater inflater;
	private DB_score db_score;
	private String name;
	
	
	public ScoreAdapter(Context context, ArrayList<String> list, DB_score db, String name){
		super(context, 0, list);
		this.context = context;
		this.list = list;
		this.inflater = LayoutInflater.from(context);
		this.db_score = db;
		this.name = name;
		//Log.i("aa","aa");
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.list_row_3, null);
		}
		String item = getItem(position);
		int win, draw, lose;
    	win=0;
    	draw=0;
    	lose=0;
		
		SQLiteDatabase db = db_score.getWritableDatabase();
		if (name!=null){
			Cursor c = db.rawQuery("SELECT * FROM db_deck WHERE name='"+name+ "'", null);
			if (c.getCount() != 0){
		    	c.moveToFirst();
				win += c.getInt(3*position+3);
				draw += c.getInt(3*position+4);
				lose += c.getInt(3*position+5);
			}
		}
		Integer win_sum = new Integer(win); 
    	Integer draw_sum = new Integer(draw); 
    	Integer lose_sum = new Integer(lose); 
    	Integer All = new Integer(win_sum.intValue()+draw_sum.intValue()+lose_sum.intValue());
    	Float win_rate;
    	if(All.intValue()!=0){
    		win_rate = new Float(100*(win_sum.floatValue()/All.floatValue()));
    	}
    	else{
    		win_rate = new Float(0);
    	}
    	Integer result_rate = new Integer(win_rate.intValue());
    	
		ImageView name = (ImageView)convertView.findViewById(R.id.name);
		TextView name2 = (TextView)convertView.findViewById(R.id.name2);
		if (item!=null || item!=""){
			name = heroimage(item, name);
			name2.setText(item);
		}
		
		
		
		TextView w = (TextView)convertView.findViewById(R.id.win);
		w.setText(win_sum.toString());
		TextView d = (TextView)convertView.findViewById(R.id.draw);
		d.setText(draw_sum.toString());
		TextView l = (TextView)convertView.findViewById(R.id.lose);
		l.setText(lose_sum.toString());
		
		TextView rate = (TextView)convertView.findViewById(R.id.rate_text);
		rate.setText(result_rate.toString()+"%");
		db.close();
		return convertView;
	}
	
	public ImageView heroimage(String hero,ImageView view){
		if(hero.equals("neutral")){
			view.setImageResource(0);
		}else if(hero.equals("druid")){
			view.setImageResource(R.drawable.hero_1s);
		}else if(hero.equals("hunter")){
			view.setImageResource(R.drawable.hero_2s);
		}else if(hero.equals("mage")){
			view.setImageResource(R.drawable.hero_3s);
		}else if(hero.equals("paladin")){
			view.setImageResource(R.drawable.hero_4s);
		}else if(hero.equals("priest")){
			view.setImageResource(R.drawable.hero_5s);
		}else if(hero.equals("rogue")){
			view.setImageResource(R.drawable.hero_6s);
		}else if(hero.equals("shaman")){
			view.setImageResource(R.drawable.hero_7s);
		}else if(hero.equals("warlock")){
			view.setImageResource(R.drawable.hero_8s);
		}else if(hero.equals("warrior")){
			view.setImageResource(R.drawable.hero_9s);
		}else{
			view.setImageResource(0);
		}
		return view;
	}
}
