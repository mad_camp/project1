package com.example.hs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;


public class GetXMLTask5 extends AsyncTask<String, Void, String> {
	private Context context;
	private ArrayList<String> card_list;
	private HashMap<String , Integer> count;

	public GetXMLTask5(Context context, ArrayList<String> card_list){
		this.context = context;
		this.card_list = card_list;
		count = new HashMap<String , Integer>();
	}

    @Override
    protected String doInBackground(String... urls) {
    	String s=null;
        for (String url : urls) {
        	downHtml("http://m.inven.co.kr/hs/deck/detail.php?idx="+url);
        	s = downHtml2("http://hs.inven.co.kr/dataninfo/deck/view.php?idx="+url);
        }
        return s;
	}
	private String downHtml(String url) {
		StringBuilder html = new StringBuilder();

		try {
			URL u = new URL(url);
			HttpURLConnection conn= (HttpURLConnection)u.openConnection();
			if (conn!=null){
				//conn.setConnectTimeout(10000);
				conn.connect();
				InputStreamReader isr = new InputStreamReader(conn.getInputStream(), "euc-kr");
				BufferedReader br = new BufferedReader(isr);
				for(;;){
					String line = br.readLine();
					if (line == null) break;
					html.append(line+"\n");
				}
				br.close();

			}
			conn.disconnect();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		String result = html.toString();
		int start = 0;
    	
    	//System.out.println("========");
		//HashMap<String , Integer> count = new HashMap<String , Integer>();
		
    	while(true){
			int pos = result.indexOf("<span class=\"name", start);
			
			if (pos == -1) break;
			for (int i=0; i<1; i++){
	    		pos = result.indexOf(">", pos);
	    		pos++;
			}
			
			int end = result.indexOf("<", pos);
			String temp = result.substring(pos, end);
			
			pos = result.indexOf("class=\"count\"", pos);
			for (int i=0; i<1; i++){
	    		pos = result.indexOf(">", pos);
	    		pos++;
			}
			pos++;
			end = result.indexOf("<", pos);
			String temp2 = result.substring(pos, end);
			
			System.out.println(temp);
			System.out.println(Integer.parseInt(temp2));
			
			count.put(temp, Integer.parseInt(temp2));
			
			
			
			start = pos+1;
    	}
    	
    	return result;
	}

	
	
	private String downHtml2(String url) {
		StringBuilder html = new StringBuilder();
		int t = 0;
		try {
			URL u = new URL(url);
			HttpURLConnection conn= (HttpURLConnection)u.openConnection();
			if (conn!=null){
				//conn.setConnectTimeout(10000);
				conn.connect();
				InputStreamReader isr = new InputStreamReader(conn.getInputStream(), "euc-kr");
				BufferedReader br = new BufferedReader(isr);
				for(;;){
					String line = br.readLine();
					if (line == null) break;
					html.append(line+"\n");
				}
				br.close();

			}
			conn.disconnect();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		String result = html.toString();
		int start = 0;
    	
    	//System.out.println("========");
		
		ArrayList<String> kor = new ArrayList<String>();
		ArrayList<String> eng = new ArrayList<String>();
		
	//	System.out.println("================");
		int pos = result.indexOf("{names:", start);
		pos +=7;
		start = pos;
		int finis = result.indexOf("}", start);
		
	//	System.out.println(pos);
		//System.out.println(finis);
    	while(true){
    		pos = result.indexOf(":'", start);
    		//System.out.println(pos);
    		start = pos+1;
    		if (pos > finis) break;
    		pos+=2;
    		int end = result.indexOf("'", pos);
			kor.add(result.substring(pos, end));
			//System.out.println(result.substring(pos, end));
		//	System.out.println(count.get(result.substring(pos, end)));
    	}
    	
    	start = result.indexOf("search:[", 0);
    	for (int i=0; i<kor.size(); i++){
    		if  (count.get(kor.get(i)) == null) System.out.println(kor.get(i));
    		int c = count.get(kor.get(i));
    		pos = result.indexOf("'", start);
    		start=pos+1;
    		/*
    		for (int j=0; j<2; j++){
	    		pos = result.indexOf("|", pos);
	    		pos++;
			} 
    		int end = result.indexOf("|", pos);
    //		System.out.println(end);
    		//System.out.println(end);
    		start = end+1;
    		for (int k=0; k<c; k++){
    			card_list.add(result.substring(pos, end));
    			
    		}*/
    		start = result.indexOf("',", start);
    		
    	//	System.out.println(result.substring(pos, start));
    		String a = result.substring(pos, start);
    		int s = a.indexOf("|", 0);
    		s++;
    		s = a.indexOf("|", s);
    		s++;
    		int e = a.indexOf("|", s);
    		if (e==-1) e = a.length();
    	//	System.out.println(a.substring(s, e));
    		for (int k=0; k<c; k++){
        		card_list.add(a.substring(s, e));
    		}
    		start++;
    		
    		
    	}
    	
    	return result;
	}
    
	// Sets the Bitmap returned by doInBackground
    @Override
    protected void onPostExecute(String result) {
    	
    }

}



