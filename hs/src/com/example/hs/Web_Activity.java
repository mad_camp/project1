package com.example.hs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


public class Web_Activity extends Activity {
	private String mInputUrl = "http://www.hearthpwn.com/decks/";
    

    private WebView mWebView;
    private WebSettings mWebSettings;
    private ProgressBar mProgressBar;
    private InputMethodManager mInputMethodManager;
 
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);

        Intent i = getIntent();
		mInputUrl = i.getExtras().getString("weburl");
        mWebView = (WebView)findViewById(R.id.web);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
         
        mInputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
         
        mWebView.setWebChromeClient(new webViewChrome());
        mWebView.setWebViewClient(new webViewClient());
        mWebSettings = mWebView.getSettings();
        mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        mWebSettings.setBuiltInZoomControls(true);
         
        mWebView.loadUrl(mInputUrl);
    }
     
     
    class webViewChrome extends WebChromeClient {
         
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            //현제 페이지 진행사항을 ProgressBar를 통해 알린다.
            if(newProgress < 100) {
                mProgressBar.setProgress(newProgress);
            } else {
                mProgressBar.setVisibility(View.INVISIBLE);
                mProgressBar.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            }
        }
    }
     
    class webViewClient extends WebViewClient {
         
        //Loading이 시작되면 ProgressBar처리를 한다.
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 15));
            view.loadUrl(url);
            return true;
        }
         
        @Override
        public void onPageFinished(WebView view, String url) {
            mWebSettings.setJavaScriptEnabled(true);
            super.onPageFinished(view, url);
        }
    }
     
    //http://를 체크하여 추가한다.
    private String httpInputCheck(String url) {
         
        if(url.indexOf("http://") == ("http://").length()) return url;
        else return "http://" + url;
    }
}