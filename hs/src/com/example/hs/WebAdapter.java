package com.example.hs;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WebAdapter extends ArrayAdapter<String> {
	private Context context;
	private ArrayList<String> list;
	private LayoutInflater inflater;
	
	public WebAdapter(Context context, ArrayList<String> list){
		super(context, 0, list);
		this.context = context;
		this.list = list;
		this.inflater = LayoutInflater.from(context);
		//Log.i("aa","aa");
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.list_row_4, null);
		}
		
		TextView name = (TextView)convertView.findViewById(R.id.name);
		String n = getItem(position);
		name.setText(n.substring(0, n.indexOf("*heRo")));
		
		ImageView hero = (ImageView)convertView.findViewById(R.id.hero_icon);
	//	System.out.println(n.substring(n.indexOf("*****"), n.length()));
		heroimage(n.substring(n.indexOf("*heRo")+5, n.length()),hero);
		return convertView;
	}
	
	
	public ImageView heroimage(String hero,ImageView view){
		if(hero.equals("neutral")){
			view.setImageResource(0);
		}else if(hero.equals("druid")){
			view.setImageResource(R.drawable.icon_1);
		}else if(hero.equals("hunter")){
			view.setImageResource(R.drawable.icon_2);
		}else if(hero.equals("mage")){
			view.setImageResource(R.drawable.icon_3);
		}else if(hero.equals("paladin")){
			view.setImageResource(R.drawable.icon_4);
		}else if(hero.equals("priest")){
			view.setImageResource(R.drawable.icon_5);
		}else if(hero.equals("rogue")){
			view.setImageResource(R.drawable.icon_6);
		}else if(hero.equals("shaman")){
			view.setImageResource(R.drawable.icon_7);
		}else if(hero.equals("warlock")){
			view.setImageResource(R.drawable.icon_8);
		}else if(hero.equals("warrior")){
			view.setImageResource(R.drawable.icon_9);
		}else{
			view.setImageResource(0);
		}
		return view;
	}
}
